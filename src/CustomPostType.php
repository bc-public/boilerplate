<?php

namespace Bci\Boilerplate;

abstract class CustomPostType
{
  protected $afterRegisterCallable;
  protected $options;
  protected $pluralPostType;
  protected $postType;
  protected string $slug;
  protected $queryValues;

  public function __construct()
  {
    $this->postType = ucfirst( $this->slug );
    $this->pluralPostType = "{$this->postType}s";

    $this->queryValues = [
      'post_type' => $this->slug
    ];
  }

  protected function after_register()
  {
    // if ( WP_DEBUG && WP_DEBUG_LOG ) {
    //   error_log( "Custom post type {$this->postType} was registered!" );
    // }
  }

  public function alphabetize_posts()
  {
    // Sort Products alphabetically
    $setProductSortOrder = function ( $query ) {
      if (
        ! is_admin() ||
        ! $query->is_main_query() ||
        ! is_object( $screen = get_current_screen() )
      ) return;

      if ( $this->slug == $screen->post_type ) {
        $query->set( 'orderby', 'title' );
        $query->set( 'order', 'ASC' );
      }
    };
    add_action( 'pre_get_posts', $setProductSortOrder );

    return $this;
  }

  public function first()
  {
    $posts = $this->get();

    if (! is_array( $posts ) || empty( $posts ) ) {
      return null;
    }

    return $posts[0];
  }

  public function get()
  {
    return get_posts( $this->queryValues );
  }

  public function register( array $removeSupport = [], array $addSupport = [], bool $tags = false )
  {
    $defaultOptions = array(
      'labels' => array(
          'name'                  => __( $this->pluralPostType ),
          'singular_name'         => __( $this->postType ),
          'add_new'               => __( "New {$this->postType}" ),
          'add_new_item'          => __( "Add New {$this->postType}" ),
          'edit_item'             => __( "Edit {$this->postType}" ),
          'new_item'              => __( "New {$this->postType}" ),
          'all_items'             => __( $this->pluralPostType ),
          'view_items'            => __( "View {$this->pluralPostType}" ),
          'search_items'          => __( "Search {$this->pluralPostType}" ),
          'not_found'             => __( "No {$this->pluralPostType} found" ),
          'not_found_in_trash'    => __( "No {$this->pluralPostType} found in trash" ),
          'menu_name'             => _x( $this->pluralPostType, $this->slug . ' post type menu name' ),
          'filter_items_list'     => __( "Filter {$this->pluralPostType} list" ),
          'items_list_navigation' => __( "{$this->pluralPostType} list navigation" ),
          'items_list'            => __( "{$this->pluralPostType} list" )
      ),
      'has_archive' => false,
      'public' => false,
      'rewrite' => array( 'slug' => strtolower( $this->pluralPostType ) ),
      'show_ui' => true,
    );

    $options = array_merge( $defaultOptions, $this->options );
    register_post_type( $this->slug, $options );

    array_walk( $removeSupport, function( $value ) {
      remove_post_type_support( $this->slug, $value );
    } );

    array_walk( $addSupport, function( $value ) {
      add_post_type_support( $this->slug, $value );
    } );

    if ( $tags ) register_taxonomy_for_object_type( 'post_tag', $this->slug );

    $this->after_register();

    return $this;
  }

  protected function set_query_value( string $key, string $value )
  {
    $this->queryValues[ 'meta_key' ] = $key;
    $this->queryValues[ 'meta_value' ] = $value;
  }

  protected function set_post_columns( callable $callable )
  {
    add_filter( 'manage_' . $this->slug . '_posts_columns', $callable );
  }

  protected function set_post_columns_values( callable $callable )
  {
    add_action( 'manage_' . $this->slug . '_posts_custom_column', $callable, 10, 2 );
  }

  protected function set_post_sortable_columns( callable $callable )
  {
    add_filter( 'manage_edit-' . $this->slug . '_sortable_columns', $callable );
  }

  protected function set_post_sort_order( string $orderBy, string $order )
  {
    add_action( 'pre_get_posts', function( $query ) use ( $orderBy, $order ) {
      if (
        ! array_key_exists( 'post_type', $query->query )
        || $query->query[ 'post_type' ] !== $this->slug
        || isset( $_GET['orderby'] )
        || isset( $_GET['order'] )
      ) return;
      
      $query->set( 'orderby', $orderBy );
      $query->set( 'order', $order );
    } );
  }

  public function slug(): string
  {
    return $this->slug;
  }

  public static function where( string $key, string $value )
  {
    $postType = new static();

    $postType->set_query_value( $key, $value );

    return $postType;
  }
}