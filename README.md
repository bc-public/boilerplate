## WP Boilerplate

### Berry Companies Inc

#### `CustomPostType` class

Extend the `CustomPostType` class to easily register custom post types. Just set the post type `slug` and override any default options in the class constructor.

**Note:** `$this->slug` must be set *before* calling `parent::__construct()`.
```php
use Bci\Boilerplate\CustomPostType;

class ExamplePostType extends CustomPostType
{
    public function __construct()
    {
        $this->slug = 'example';

        parent::__construct();

        $this->options = array(
          'menu_icon' => 'dashicons-location'
          'labels' => array(
            'name' => 'Example'
          )
        );

        return $this;
    }
}
```

Need to run code after your post type is registered? Just define an `after_register()` method:
```php
  protected function after_register()
  {
    // This method will run after register() is called.
  }
```

In your `functions.php` file, instantiate your new post type and call `register()`:
```php
  // functions.php
  <?php

  require_once(__DIR__ . '/vendor/autoload.php');

  // ...

  add_action( 'init', function () {
    // Just register the post type
    ( new ExamplePostType )->register();

    // Register post type and alphabetize the posts in the Admin
    ( new ExamplePostType )->register()->alphabetize_posts();

    // Remove and add support in the register() method call.
    ( new ExamplePostType )->register(
      $remove = [ 'editor', 'comment' ],
      $add = [ 'revisions' ],
      $tags = true|false
    );
    
  })
```
